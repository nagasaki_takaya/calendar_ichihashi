<?php
function getDb() {
    $dsn = 'mysql:dbname=calendarholiday;host=localhost;charset=utf8;';
    $usr = 'root';
    $password = '';

    try {
        $db = new PDO ( $dsn, $usr, $password );
    } catch ( PDOException $e ) {
        die ( "接続エラー:({$e->getMessage()}" );
    }
    return $db;
}

$db = getDb();