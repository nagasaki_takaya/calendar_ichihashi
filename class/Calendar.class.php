<?php
require_once __DIR__ . '/../function/Encode.php';
require_once __DIR__ . '/../function/getDb.php';

class Calendar {

    public $holidayJs = array();
    public $calendarYear;
    protected $weeks;
    protected $holidays = array();

    public function __construct($year) {
        $this->calendarYear = $year;
        $db = getDb();
        $sql = "SELECT * FROM holidaytime WHERE date >= ? && date < ?";
        $stt = $db->prepare($sql);
        $stt->bindValue(1, "$year-01-01");
        $stt->bindValue(2, ($year + 1) . "-01-01");
        $stt->execute();
        $db = null;

        while ($row = $stt->fetch()) {
            $this->holidays[] = $row;
        }
        return;
    }

    public function create($mon) {
        $year = $this->calendarYear;
        $lastDay = date("t", strtotime($year . "-" . $mon . "-1")); // 引数t 日数28~31
        $youbi = date("w", strtotime($year . "-" . $mon . "-1")); // 引数w 曜日　数値0~6 [ISO-8601~ 引数Nで1~7]
        $holidays = $this->holidays;
        $this->weeks = array();
        $week = "";
        $week .= str_repeat('<td></td>', $youbi);
        foreach ($holidays as $holiday) {
            $holidayAryDate[] = $holiday['date'];
        }

        for ($day = 1; $day <= $lastDay; $day ++, $youbi ++) {
            $datetime = new DateTime("$year-$mon-$day");
            $currentDate = $datetime->format('Y-m-d'); // Y-m-d(2015-03-05)
            $holidayNo = array_search($currentDate, $holidayAryDate); // 戻り値(キーの値/false) 1日は戻り値0なので注意
            if ((boolean) $holidayNo === false && $holidayNo !== 0) {
                $week .= sprintf('<td class="youbi_%d">%d</td>', $youbi % 7, $day);
            } else {
                $week .= sprintf('<td class="youbi_%d holi" onmouseover="showPopup(event,\'%d_%d\');" onmouseout="hidePopup(\'%d_%d\');">%d</td>', $youbi % 7, $mon, $day, $mon, $day, $day); // マウスオーバー処理
                $this->holidayJs[] .= sprintf('<div class="holidayId" id=%d_%d>%s</div>', $mon, $day, $holidays["$holidayNo"]['name']); // 祝日にID付与
            }
            if (($youbi % 7 == 6) || ($day == $lastDay)) { // 最終週の処理
                $week .= str_repeat('<td></td>', 6 - ($youbi % 7));
                $this->weeks[] = '<tr>' . $week . '</tr>';
                $week = '';
            }
        }
    }

    public function getWeeks() {
        return $this->weeks;
    }

    public function holidayJs() {
        return $this->holidayJs;
    }
}
