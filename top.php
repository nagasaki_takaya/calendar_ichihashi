<?php
require_once __DIR__ . '/class/Calendar.class.php';
require_once __DIR__ . '/function/Encode.php';

$year = isset($_GET['y']) ? $_GET['y'] : date('Y');
$cal = new Calendar($year); // 対象年のオブジェクト$cal生成
?>
<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="utf-8">
    <title>PHPでカレンダー</title>
    <link rel="stylesheet" href="css/style.css">
    <script type="text/javascript" src="js/popUp.js"></script>
</head>
<body>
    <table id="frame">
        <thead>
            <!--headerここから -->
            <tr>
                <th colspan="3"><a href="?y=<?php echo h($year-1);?>">&laquo;</a>
                <?php echo h($year); ?>
                <a href="?y=<?php echo h($year+1);?>">&raquo;</a></th>
            </tr>
        </thead>
        <!--headerここまで-->
        <tbody>  <!-- bodyここから -->
            <tr>
<?php
for ($itr = 1; $itr <= 12; $itr++) :   // 月の生成
    $cal->create($itr);
?>
                <td>
                    <table class="month">   <!--monthここから-->
                        <thead>
                            <tr>
                                <th colspan="7"><?php echo $itr.'月'; ?></th>
                            </tr>
                            <tr>
                                <th>日</th>
                                <th>月</th>
                                <th>火</th>
                                <th>水</th>
                                <th>木</th>
                                <th>金</th>
                                <th>土</th>
                            </tr>
                        </thead>
                        <tbody>
<?php
    foreach ($cal->getWeeks() as $week) {
        echo $week; // week(日付)表示
    }
?>
                        </tbody>
                    </table>   <!--monthここまで-->
                </td>


<?php
    if ($itr % 3 == 0) {
        echo "</tr><tr>";
    } elseif ($itr == 12) {
        echo "</tr>";
    }
endfor;
?>
        </tbody> <!-- bodyここまで -->
    </table> <!-- frameここまで -->


<!-- JSポップ用 ID付与 style hide -->
<?php
foreach ($cal->holidayJs as $holidayJ) {
    echo $holidayJ;
}
?>
<!-- style hide ここまで -->
</body>
</html>